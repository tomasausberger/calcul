/* Need parser.js */

function calculBrain(){    
    this.listening = true;
    this.clear = false;
    this.defined = null;
    this.commands = [];
    this.lastNumber = [];
    this.lastNumberNegative = false;
    this.lastNumberVariable = false;
    this.error = null;
    
    this.objects = [];
    
    this.functions = [];
    
    //constants
    this.TYPE_NULL = 0;
    this.TYPE_ADD = 1;
    this.TYPE_GAIN = 2;
    this.TYPE_DIVIDE = 3;
    this.TYPE_POW = 4;
    this.TYPE_ROOT = 5;
    this.TYPE_LOG = 6;
    this.TYPE_FACT = 7;
    this.TYPE_COMB = 8;
    this.TYPE_GONIO = 9;
    this.TYPE_NEG = 10;
    this.TYPE_ABS = 11;
    
    this.GONIO_SIN = 1;
    this.GONIO_ARCSIN = 2;
    this.GONIO_COS = 3;
    this.GONIO_ARCCOS = 4;
    this.GONIO_TAN = 5;
    this.GONIO_ARCTAN = 6;
    
    this.startListening = function(){
        this.listening = true;
    }
    
    this.stopListening = function(){
        this.listening = false;
    }
    
    this.sendData = function(data){
        var self = this;
        data.forEach(function(value){
            //listening
            if(value == "start"){
                self.startListening();
                return true;
            }
            if(!self.listening){
                return true;
            }else{
                if(value == "pause"){
                    self.stopListening();
                    return true;
                }
            }
            
            if(self.error !== null && (value != "=" && value != "def" && value != "use")){
                return false;
            }
            
            if(value == "use"){
                if(self.lastNumber.length != 0 || self.commands.length != 0 || self.error !== null){
                    self.end();
                }
                self.useFunction = true;
                return true;
            }else if(value == "as"){
                self.defined = self.parseNumber();
                return true;
            }else if(value == "x"){
                if(self.useFunction && self.usedFunction === null){
                    try{
                        if(self.lastNumber.length != 0){
                            self.usedFunction = self.parseNumber();
                            if(!(self.usedFunction in self.functions)){
                                throw "Calcul brain error: undefined function";
                            }
                        }else{
                            throw "Calcul brain error: void function";
                        }
                    }catch(e){
                        self.error = e;
                    }
                }else{
                    if(self.lastNumber.length != 0){
                        self.commands.push(self.parseNumber());
                    }
                }
                self.lastNumberVariable = true;
                return true;
            }
            
            if(self.clear){
                self.objects = [];
                self.clear = false;
            }
            
            //make command
            if(value == "=" || value == "def"){
                if(self.lastNumber.length != 0 || self.commands.length != 0 || self.error !== null){
                    self.end();
                }
            }else if(self.isNumber(value) || value == "." || value == "neg" || value == "e" || value == "pi" || value == "M" || value == "k"){
                if(value == "neg"){
                    self.lastNumberNegative = true;
                }else{
                    self.lastNumber.push(value);
                }
            }else{
                if(self.lastNumber.length != 0){
                    self.commands.push(self.parseNumber());
                }
                self.commands.push(value);
            }
        });
        if(self.lastNumber.length != 0 || self.commands.length != 0 || self.error !== null){
            self.end();
        }
        self.clear = true;
        return self.objects;
    }
    
    this.end = function(){
        var newObject = {};
        if(this.lastNumber.length != 0){
            this.commands.push(this.parseNumber());
        }
        
        if(!this.useFunction && this.error === null){
            this.commands.push("=");
        }

        newObject.tags = this.commands;
        try{
            if(this.error !== null){
                throw this.error;
            }
            if(this.useFunction){
                newObject.arguments = this.getArguments();
            }else{
                newObject.tree = this.getTree(this.commands);
                this.validateTree(newObject.tree);
                newObject.lisp = this.getLisp(newObject.tree);
                newObject.command = this.getCommand(newObject.tree);
            }
            if(this.defined === null && !this.useFunction){
                newObject.value = Parser.evaluate(newObject.command);
                newObject.functionIndex = this.defined;
                newObject.tex = this.getTex(newObject.tree, null, newObject.value, null);
            }else if(this.useFunction){
                newObject.functionIndex = this.usedFunction;
                newObject.value = this.functions[this.usedFunction].defined.evaluate(newObject.arguments);
                newObject.lisp = this.functions[this.usedFunction].lisp;
                newObject.tex = this.getTex(this.functions[this.usedFunction].tree, this.usedFunction, newObject.value, newObject.arguments);
            }else{
                newObject.value = null;
                newObject.defined = Parser.parse(newObject.command);
                newObject.functionIndex = this.defined;
                newObject.tex = this.getTex(newObject.tree, newObject.functionIndex, null, null);
            }
            
            newObject.valid = true;
        }catch(e){
            newObject.error = e;
            newObject.valid = false;
        }
        if(this.defined !== null && newObject.valid){
            this.functions[this.defined] = newObject;
        }
        
        this.defined = null;
        this.useFunction = false;
        this.usedFunction = null;
        this.commands = [];
        this.error = null;
        this.objects.push(newObject);
    }
    
    this.getArguments = function(){
        var args = {};
        var lastArg = null;
        var self = this;
        this.commands.forEach(function(value){
            if(value == ":"){
                return true;
            }
            if(lastArg === null){
                if(self.isVariable(value)){
                    lastArg = value;
                }else{
                    throw "Calcul brain exception: No variable defined.";
                }
            }else{
                if(self.isNumber(value) || value == "e" || value == "pi" || value == "E" || value == "PI"){
                    if(value == "e" || value == "E"){
                        value = Math.E;
                    }else if(value == "pi" || value == "PI"){
                        value = Math.PI;
                    }
                    args[lastArg] = value;
                    lastArg = null;
                }else{
                    throw "Calcul brain exception: No variable value defined.";
                }
            }
        });
        return args;
    }
    
    this.getTree = function(commands){
        var commandTree = {type: this.TYPE_NULL, args: [], parent: null, inner: false, need: false};
        var self = this;
        var open = commandTree;
        var addTag, reverse, close = false, closeTag, deep, gonioType, findElementType, find, parent, second;

        commands.forEach(function(tag){
            addTag = null;
            deep = false;
            reverse = false;
            findElementType = null;
            
            //end
            if(close){
                return true;
            }else if(tag == "="){
                find = open;
                do{
                    if(find.need){
                        throw "Calcul tree parse error: Cannot end, element missing argument";
                    }
                    find = find.parent;
                }while(find !== null);
                close = true;
                return true;
            }
            
            //if don't need reverse find
            if(tag != "/" && tag != "rt" && tag != "^" && tag != "base" && tag != "from" && tag != "!" && tag != "over"){
                //close full elements
                find = open;
                do{
                    if(find.need || find.inner){
                        break;
                    }
                    if(find.type == self.TYPE_GAIN || find.type == self.TYPE_NULL || find.type == self.TYPE_ADD){
                        break;
                    }else if(find.args.length == 0 && (find.type == self.TYPE_GONIO || find.type == self.TYPE_FACT || find.type == self.TYPE_NEG || find.type == self.TYPE_ABS)){
                        break;
                    }else if(find.args.length < 2 && (find.type == self.TYPE_COMB || find.type == self.TYPE_DIVIDE || find.type == self.TYPE_POW || find.type == self.TYPE_ROOT)){
                        break;
                    }if(find.args.length == 0 && find.type == self.TYPE_LOG){
                        break;
                    }
                    find = find.parent;
                }while(find !== null);
                if(find !== null){
                    open = find;
                }else{
                    open = commandTree;
                }
            }

            //create
            if(tag == "-"){
                addTag = {type: self.TYPE_NEG, args: [], parent: null, inner: false, need: true};
                if(open.type == self.TYPE_NULL){
                    open.type = self.TYPE_ADD;
                }
            }else if(tag.charAt(0) == "-"){
                //number start with -
                addTag = {type: self.TYPE_NEG, args: [tag.slice(1)], parent: null, inner: false, need: false};
                if(open.type == self.TYPE_NULL){
                    open.type = self.TYPE_ADD;
                }
            }else if(tag == "abs"){
                addTag = {type: self.TYPE_ABS, args: [], parent: null, inner: false, need: true};
            }else if(tag == "+"){
                if(open.type == self.TYPE_ADD){
                    open.need = true;
                    return true;
                }else if(open.type == self.TYPE_NULL){
                    open.type = self.TYPE_ADD;
                    open.need = true;
                    return true;
                }else{
                    findElementType = self.TYPE_ADD;
                }
            }else if(tag == "*"){
                if(open.type == self.TYPE_GAIN){
                    open.need = true;
                    return true;
                }else if(open.type == self.TYPE_NULL){
                    open.type = self.TYPE_GAIN;
                    open.need = true;
                    return true;
                }else if(open.type == self.TYPE_ADD){
                    addTag = {type: self.TYPE_GAIN, args: [], parent: null, inner: false, need: true};
                    reverse = true;
                }else{
                    findElementType = self.TYPE_GAIN;
                }
            }else if(tag == "lz"){
                addTag = {type: self.TYPE_NULL, args: [], parent: null, inner: true, need: true};
            }else if(tag == "/"){
                if(open.type == self.TYPE_NULL){
                    open.type = self.TYPE_DIVIDE;
                    open.need = true;
                    return true;
                }else{
                    addTag = {type: self.TYPE_DIVIDE, args: [], parent: null, inner: false, need: true};
                    reverse = true;
                }
            }else if(tag == "rt"){
                if(open.type == self.TYPE_NULL){
                    open.type = self.TYPE_ROOT;
                    open.need = true;
                    if(open.args.length == 0){
                        open.args.push(2);
                    }
                    return true;
                }else{
                    addTag = {type: self.TYPE_ROOT, args: [], parent: null, inner: false, need: true};
                    reverse = true;
                }
            }else if(tag == "^"){
                if(open.type == self.TYPE_NULL){
                    open.type = self.TYPE_POW;
                    open.need = true;
                    return true;
                }else{
                    addTag = {type: self.TYPE_POW, args: [], parent: null, inner: false, need: true};
                    reverse = true;
                }
            }else if(tag == "log"){
                if(open.type == self.TYPE_NULL){
                    open.type = self.TYPE_GAIN;
                }
                addTag = {type: self.TYPE_LOG, args: [], parent: null, inner: false, need: true};
            }else if(tag == "!"){
                if(open.type == self.TYPE_NULL){
                    open.type = self.TYPE_FACT;
                    return true;
                }else{
                    addTag = {type: self.TYPE_FACT, args: [], parent: null, inner: false, need: false};
                    reverse = true;
                }
            }else if(tag == "komb"){
                if(open.type == self.TYPE_NULL){
                    open.type = self.TYPE_GAIN;
                }
                addTag = {type: self.TYPE_COMB, args: [], parent: null, inner: false, need: true};
            }else if(tag == "over"){
                if(open.type == self.TYPE_NULL){
                    open.type = self.TYPE_COMB;
                    open.need = true;
                    return true;
                }else{
                    findElementType = self.TYPE_COMB;
                }
            }else if(tag == "sin" || tag == "cos" || tag == "tan" || tag == "arcsin" || tag == "arccos" || tag == "arctan"){
                if(tag == "sin"){
                    gonioType = self.GONIO_SIN;
                }else if(tag == "cos"){
                    gonioType = self.GONIO_COS;
                }else if(tag == "tan"){
                    gonioType = self.GONIO_TAN;
                }else if(tag == "arcsin"){
                    gonioType = self.GONIO_ARCSIN;
                }else if(tag == "arccos"){
                    gonioType = self.GONIO_ARCCOS;
                }else{
                    gonioType = self.GONIO_ARCTAN;
                }
                if(open.type == self.TYPE_NULL){
                    open.type = self.TYPE_GAIN;
                }
                addTag = {type: self.TYPE_GONIO, args: [], parent: null, inner: false, gonioType: gonioType, need: true};
            }else if(tag == "base" || tag == "from"){
                findElementType = self.TYPE_LOG;
            }

            //find element and use it
            if(findElementType !== null){
                find = open;
                do{
                    if(find.type == findElementType){
                        open = find;
                        break;
                    }
                    if(findElementType == self.TYPE_GAIN && find.type == self.TYPE_ADD){
                        addTag = {type: self.TYPE_GAIN, args: [], parent: null, inner: false, need: true};
                        reverse = true;
                        break;
                    }
                    if(find.need || find.inner || find.parent === null){
                        if(findElementType == self.TYPE_COMB){
                            addTag = {type: self.TYPE_COMB, args: [], parent: null, inner: false, need: true};
                            reverse = true;
                            break;
                        }else if(findElementType == self.TYPE_ADD || findElementType == self.TYPE_GAIN){
                            //change with parent <->
                            if(find.inner || find.parent === null){
                                parent = find.parent;
                                if(find.parent !== null && parent.args.pop() !== find){
                                    throw "Calcul tree parse error: Switched element is not the last";
                                }
                                addTag = {type: findElementType, args: [], parent: find.parent, inner: true, need: true};
                                find.parent = addTag;
                                find.inner = false;
                                addTag.args.push(find);
                                if(parent === null){
                                    commandTree = addTag;
                                    open = commandTree;
                                    return true;
                                }
                                open = parent;
                                break;
                            }else{
                                throw "Calcul tree parse error: Not closeable element";
                            }
                        }else if(findElementType == self.TYPE_LOG){
                            throw "Calcul tree parse error: Log not found";
                        }
                    }
                    find = find.parent;
                }while(true);
                if(addTag === null){
                    if(findElementType == self.TYPE_LOG || findElementType == self.TYPE_COMB){
                        open.need = true;
                    }
                    return true;
                }
            }else if(tag == "pz"){
                find = open;
                do{
                    if(find.need){
                        throw "Calcul tree parse error: Not closeable element";
                    }
                    if(find.parent === null){
                        open = find;
                        break;
                    }
                    if(find.inner){
                        open = find.parent;
                        break;
                    }
                    find = find.parent;
                }while(true);
                return true;
            }

            //add
            if(addTag !== null || self.isNumberOrVar(tag)){
                if(reverse){
                    if(addTag.type == self.TYPE_ROOT && open.need){
                        if(open.need){
                            open.need = false;
                            addTag.args.push(2);
                        }else{
                            find = open.args.pop();
                            if(typeof find === "object"){
                                find.parent = addTag;
                            }
                            addTag.args.push(find);
                        }
                        addTag.parent = open;
                        open.args.push(addTag);
                        open = addTag;
                    }else{
                        find = open.args.pop();
                        if(typeof find === "object"){
                            find.parent = addTag;
                        }
                        addTag.parent = open;
                        addTag.args.push(find);
                        open.args.push(addTag);
                        open = addTag;
                    }
                }else{
                    open.need = false;
                    if(open.type == self.TYPE_ADD || open.type == self.TYPE_GAIN || open.type == self.TYPE_NULL){
                        if(addTag === null){
                            open.args.push(tag);
                        }else{
                            addTag.parent = open;
                            open.args.push(addTag);
                            open = addTag;
                        }
                    }else if(open.type == self.TYPE_GONIO || open.type == self.TYPE_NEG || open.type == self.TYPE_ABS){
                        if(open.args.length == 0){
                            if(addTag === null){
                                open.args.push(tag);
                            }else{
                                addTag.parent = open;
                                open.args.push(addTag);
                                open = addTag;
                            }
                        }else{
                            throw "Calcul tree parse error: Too many params for parse (gonio, neg, abs)";
                        }
                    }else if(open.type == self.TYPE_DIVIDE || open.type == self.TYPE_POW || open.type == self.TYPE_ROOT){
                        if(open.args.length == 1){
                            if(addTag === null){
                                open.args.push(tag);
                            }else{
                                addTag.parent = open;
                                open.args.push(addTag);
                                open = addTag;
                            }
                        }else{
                            throw "Calcul tree parse error: Not enought params (divide, power, root)";
                        }
                    }else if(open.type == self.TYPE_COMB){
                        if(open.args.length < 2){
                            if(open.args.length == 0){
                                open.need = true;
                            }
                            if(addTag === null){
                                open.args.push(tag);
                            }else{
                                addTag.parent = open;
                                open.args.push(addTag);
                                open = addTag;
                            }
                        }else{
                            throw "Calcul tree parse error: Too many params (comb)";
                        }
                    }else if(open.type == self.TYPE_LOG){
                        if(open.args.length < 2){
                            if(addTag === null){
                                open.args.push(tag);
                            }else{
                                addTag.parent = open;
                                open.args.push(addTag);
                                open = addTag;
                            }
                        }else{
                            throw "Calcul tree parse error: Too many params (log)";
                        }
                    }else if(open.type == self.TYPE_FACT){
                        if(open.args.length == 1){
                            parent = open.parent;
                            if(parent === null){
                                commandTree = {type: self.TYPE_GAIN, args: [open], parent: null, inner: false, need: false};
                                if(addTag === null){
                                    commandTree.args.push(tag);
                                }else{
                                    addTag.parent = commandTree;
                                    commandTree.args.push(addTag);
                                    open = addTag;
                                }
                            }else{
                                parent.args.pop();
                                second = {type: self.TYPE_GAIN, args: [open], parent: parent, inner: false, need: false};
                                if(addTag === null){
                                    second.args.push(tag);
                                }else{
                                    addTag.parent = second;
                                    second.args.push(addTag);
                                    open = addTag;
                                }
                                parent.args.push(second);
                            }
                        }else{
                            throw "Calcul tree parse error: Too many params (log)";
                        }
                    }else{
                        throw "Calcul tree parse error: unknown type in add tag";
                    }
                }
            }
        });

        //cut the tree in blank places
        var treeElements = [commandTree];
        var element, stop, index, switched;
        do{
            element = treeElements.pop();
            element.args.forEach(function(arg){
                if(typeof arg === "object"){
                    treeElements.push(arg);
                }
            });
            if(element.type == self.TYPE_NULL){
                if(element.args.length > 1){
                    element.type = self.TYPE_GAIN;
                }else{
                    if(element.parent === null){
                        if(element.args.length == 0){
                            throw "Calcul tree cut error: void tag";
                        }
                        if(typeof element.args[0] === "object"){
                            switched = element.args[0];
                            switched.parent = null;
                            commandTree = switched;
                        }else{
                            element.type = self.TYPE_ADD;
                        }
                    }else{
                        stop = false;
                        parent = element.parent;
                        index = 0;
                        element.parent.args.forEach(function(child){
                            if(stop){
                                return true;
                            }
                            if(child === element){
                                stop = true;
                                return true;
                            }
                            index++;
                        });
                        if(index == parent.args.length){
                            throw "Calcul tree cut error: finded tag not in parents args";
                        }
                        if(element.args.length == 0){
                            throw "Calcul tree cut error: void tag";
                        }
                        switched = element.args[0];
                        switched.parent = element.parent;
                        if(element.inner){
                            switched.inner = true;
                        }
                        element = null;
                        parent.args[index] = switched;
                    }
                }
            }else if(element.type == self.TYPE_LOG){
                if(element.args.length == 1){
                    switched = element.args.pop();
                    element.args.push(10);
                    element.args.push(switched);
                }else if(element.args.length == 0){
                    throw "Calcul tree cut error: not enought params for logarithm";
                }
            }
        }while(treeElements.length > 0);
        
        return commandTree;
    }
    
    this.validateTree = function(commandTree){
        var treeElements = [commandTree];
        var element;
        do{
            element = treeElements.pop();
            element.args.forEach(function(arg){
                if(typeof arg === "object"){
                    treeElements.push(arg);
                }
            });
            if(element.args.length == 0){
                throw "Calcul tree validation: void tag";
            }else if(element.args.length > 2 && (element.type != this.TYPE_GAIN && element.type != this.TYPE_ADD)){
                throw "Calcul tree validation: more than two params";
            }else if(element.args.length != 1 && (element.type == this.TYPE_FACT || element.type == this.TYPE_GONIO || element.type == this.TYPE_NEG || element.type == this.TYPE_ABS)){
                throw "Calcul tree validation: more than one params";
            }else if(element.args.length != 2 && (element.type == this.TYPE_DIVIDE || element.type == this.TYPE_COMB || element.type == this.TYPE_LOG || element.type == this.TYPE_POW || element.type == this.TYPE_ROOT)){
                throw "Calcul tree validation: not two params";
            }
        }while(treeElements.length > 0);
    }
    
    this.getLisp = function(element){
        var self = this;
        var lisp = "(";
        if(element.type == this.TYPE_ADD){
            lisp += "+ "
        }else if(element.type == this.TYPE_ABS){
            lisp += "ABS "
        }else if(element.type == this.TYPE_COMB){
            lisp += "COMB "
        }else if(element.type == this.TYPE_DIVIDE){
            lisp += "/ "
        }else if(element.type == this.TYPE_FACT){
            lisp += "! "
        }else if(element.type == this.TYPE_GAIN){
            lisp += "* "
        }else if(element.type == this.TYPE_LOG){
            lisp += "log "
        }else if(element.type == this.TYPE_NEG){
            lisp += "- "
        }else if(element.type == this.TYPE_POW){
            lisp += "^ "
        }else if(element.type == this.TYPE_ROOT){
            lisp += "ROOT "
        }else if(element.type == this.TYPE_GONIO){
            if(element.gonioType == this.GONIO_ARCCOS){
                lisp += "arccos "
            }else if(element.gonioType == this.GONIO_ARCSIN){
                lisp += "arcsin "
            }else if(element.gonioType == this.GONIO_ARCTAN){
                lisp += "arctan "
            }else if(element.gonioType == this.GONIO_COS){
                lisp += "cos "
            }else if(element.gonioType == this.GONIO_SIN){
                lisp += "sin "
            }else if(element.gonioType == this.GONIO_TAN){
                lisp += "tan "
            }
        }
        element.args.forEach(function(arg){
            if(typeof arg == "object"){
                lisp += self.getLisp(arg) + " ";
            }else{
                lisp += arg+" ";
            }
        });
        lisp += ")";
        return lisp;
    }
    
    this.getCommand = function(element){
        var command = "";
        var self = this;
        var first, value, secondValue;
        if(element.type == this.TYPE_ADD || element.type == this.TYPE_GAIN){ 
            first = true;
            element.args.forEach(function(arg){
                if(first){
                    first = false;
                }else{
                    if(element.type == self.TYPE_ADD){
                        command += "+";
                    }else{
                        command += "*";
                    }
                }
                if(typeof arg == "object"){
                    command += "("+self.getCommand(arg)+")";
                }else{
                    command += arg;
                }
            });
        }else if(element.type == this.TYPE_NEG){
            if(typeof element.args[0] == "object"){
                command = "("+self.getCommand(element.args[0])+")";
            }else{
                command = element.args[0];
            }
            command = "-"+command;
        }else if(element.type == this.TYPE_COMB || element.type == this.TYPE_LOG || element.type == this.TYPE_DIVIDE || element.type == this.TYPE_ROOT || element.type == this.TYPE_POW){
            if(typeof element.args[0] == "object"){
                value = "("+self.getCommand(element.args[0])+")";
            }else{
                value = element.args[0];
            }
            if(typeof element.args[1] == "object"){
                secondValue = "("+self.getCommand(element.args[1])+")";
            }else{
                secondValue = element.args[1];
            }
            if(element.type == this.TYPE_COMB){
                command = "fac("+value+")/(fac("+value+"-"+secondValue+")*fac("+secondValue+"))";
            }else if(element.type == this.TYPE_DIVIDE){
                command = value+"/"+secondValue;
            }else if(element.type == this.TYPE_LOG){
                command = "log("+secondValue+")";
                if(value != "E"){
                    command += "/log("+value+")";
                }
            }else if(element.type == this.TYPE_ROOT){
                if(value == 2){
                    command = "sqrt("+secondValue+")";
                }else{
                    command = secondValue+"^(1/"+value+")";
                }
            }else if(element.type == this.TYPE_POW){
                command = value+"^"+secondValue;
            }
        }else{
            if(typeof element.args[0] == "object"){
                value = this.getCommand(element.args[0]);
            }else{
                value = element.args[0];
            }
            if(element.type == this.TYPE_GONIO){
                if(element.gonioType == this.GONIO_ARCCOS){
                    command = "acos";
                }else if(element.gonioType == this.GONIO_ARCSIN){
                    command = "asin";
                }else if(element.gonioType == this.GONIO_ARCTAN){
                    command = "atan";
                }else if(element.gonioType == this.GONIO_COS){
                    command = "cos";
                }else if(element.gonioType == this.GONIO_SIN){
                    command = "sin";
                }else if(element.gonioType == this.GONIO_TAN){
                    command = "tan";
                }
                command += "("+value+")";
            }else if(element.type == self.TYPE_FACT){
                command = "fac("+value+")";
            }else{
                command = "abs("+value+")";
            }
        }
        return command;
    }
    
    this.getTexCommand = function(element){
        var tex = "";
        var self = this;
        var first, value, secondValue;
        if(element.type == this.TYPE_ADD || element.type == this.TYPE_GAIN){ 
            first = true;
            element.args.forEach(function(arg){
                if(first){
                    first = false;
                }else{
                    if(element.type == self.TYPE_ADD){
                        if(typeof arg != "object" || arg.type != self.TYPE_NEG){
                            tex += "+";
                        }
                    }else{
                        tex += "\\cdot ";
                    }
                }
                if(typeof arg == "object"){
                    if(arg.inner){
                        tex += "("+self.getTexCommand(arg)+")";
                    }else{
                        tex += self.getTexCommand(arg);
                    }
                }else{
                    tex += arg;
                }
            });
        }else if(element.type == this.TYPE_NEG){
            if(typeof element.args[0] == "object"){
                if(element.args[0].inner){
                    tex = "("+self.getTexCommand(element.args[0])+")";
                }else{
                    tex = self.getTexCommand(element.args[0]);
                }
            }else{
                tex = element.args[0];
            }
            tex = "-"+tex;
        }else if(element.type == this.TYPE_COMB || element.type == this.TYPE_LOG || element.type == this.TYPE_DIVIDE || element.type == this.TYPE_ROOT || element.type == this.TYPE_POW){
            if(typeof element.args[0] == "object"){
                if(element.args[0].inner){
                    value = "("+self.getTexCommand(element.args[0])+")";
                }else{
                    value = self.getTexCommand(element.args[0]);
                }
            }else{
                value = element.args[0];
            }
            if(typeof element.args[1] == "object"){
                if(element.args[1].inner){
                    secondValue = "("+self.getTexCommand(element.args[1])+")";
                }else{
                    secondValue = self.getTexCommand(element.args[1]);
                }
            }else{
                secondValue = element.args[1];
            }
            if(element.type == this.TYPE_COMB){
                tex = "({{"+value+"}\\atop{"+secondValue+"}})";//!!
            }else if(element.type == this.TYPE_DIVIDE){
                tex = "{{"+value+"}\\over{"+secondValue+"}}";
            }else if(element.type == this.TYPE_LOG){
                tex = "log";
                if(value != "10"){
                    tex += "_{"+value+"}";
                }
                if(this.isVariable(secondValue)){
                    tex += "{("+secondValue+")}";
                }else{
                    tex += "{"+secondValue+"}";
                }
            }else if(element.type == this.TYPE_ROOT){
                tex = "\\sqrt"
                if(value != 2){
                    tex += "["+value+"]";
                }
                tex += "{"+secondValue+"}";
            }else if(element.type == this.TYPE_POW){
                tex = "{"+value+"}^{"+secondValue+"}";
            }
        }else if(element.type == this.TYPE_GONIO || element.type == this.TYPE_ABS){
            if(typeof element.args[0] == "object"){
                value = self.getTexCommand(element.args[0]);
            }else{
                value = element.args[0];
            }
            if(element.type == this.TYPE_GONIO){
                if(element.gonioType == this.GONIO_ARCCOS){
                    tex = "acos";
                }else if(element.gonioType == this.GONIO_ARCSIN){
                    tex = "asin";
                }else if(element.gonioType == this.GONIO_ARCTAN){
                    tex = "atan";
                }else if(element.gonioType == this.GONIO_COS){
                    tex = "cos";
                }else if(element.gonioType == this.GONIO_SIN){
                    tex = "sin";
                }else if(element.gonioType == this.GONIO_TAN){
                    tex = "tan";
                }
                tex += "("+value+")";
            }else{
                tex = "|"+value+"|";
            }
        }else{
            if(typeof element.args[0] == "object"){
                value = "("+self.getTexCommand(element.args[0])+")";
            }else{
                value = element.args[0];
            }
            tex = value+"!";
        }
        return tex;
    }
    
    this.getTex = function(commandTree, functionIndex, value, arguments){
        var tex = "";
        var index, first = true;
        if(functionIndex){
            tex = "f_{"+functionIndex+"}";
                if(arguments !== null){
                    tex += "(";
                    for(var index in arguments) {
                        if(first){
                            first = false;
                        }else{
                            tex += ", ";
                        }
                        tex += index+"="+arguments[index];
                    }
                    tex += ")";
                }
            tex += "=";
        }
        tex += this.getTexCommand(commandTree);
        if(value){
            if(value == "Infinity"){
                tex += "=\\infty"
            }else{
                tex += "="+value;
            }
        }
        return tex;
    }
    
    this.parseNumber = function(){
        var number;
        if(this.lastNumberVariable){
            number = "x"+this.parseNumberPart(this.lastNumber).toString();
        }else{
            if(this.lastNumber.length == 1 && (this.lastNumber[0] == "pi" || this.lastNumber[0] == "e")){
                if(this.lastNumber[0] == "pi"){
                    number = "PI";
                }else if(this.lastNumber[0] == "e"){
                    number = "E";
                }
            }else{
                var isDouble = this.lastNumber.indexOf(".");
                if(isDouble > -1){
                    var intPart = this.parseNumberPart(this.lastNumber.slice(0, isDouble));
                    var doublePart = this.parseNumberPart(this.lastNumber.slice(isDouble+1, this.lastNumber.length));
                    number = parseFloat(intPart+"."+doublePart);
                }else{
                    number = this.parseNumberPart(this.lastNumber);
                }
            }
            number = number.toString();

            if(this.lastNumberNegative){
                number = "-"+number;
            }
        }
        
        this.lastNumber = [];
        this.lastNumberNegative = false;
        this.lastNumberVariable = false;
            
        return number;
    }
    
    this.parseNumberPart = function(numbers){
        var oneNumber = true;
        var allNumber;
        var self = this;
        
        numbers.forEach(function(num){
            if(oneNumber && !self.isDigit(num)){
                oneNumber = false;
            }
        });
        if(oneNumber){
            allNumber = ""
            numbers.forEach(function(num){
                allNumber = allNumber + num.toString();
            });
        }else{
            allNumber = 0;
            var partNumber = 0;
            numbers.forEach(function(num){
                if(num == "k" || num == "M"){
                    if(num == "k"){
                        partNumber = partNumber*1000;
                        allNumber += partNumber;
                        partNumber = 0;
                    }else{
                        partNumber = partNumber*1000000;
                        allNumber += partNumber;
                        partNumber = 0;
                    }
                }else{
                    partNumber += parseInt(num);
                }
            });
            allNumber += partNumber;
        }
        return allNumber.toString();
    }
    
    this.isDigit = function(string){
        var regex = /^\d$/;
        return regex.test(string);
    }
    
    this.isNumber = function(string){
        var regex = /^\d+$/;
        return regex.test(string);
    }
    
    this.isNumberOrVar = function(string){
        var regexNum = /^-?\d+$/;
        var regexVar = /^-?x\d+$/;
        return (regexNum.test(string) || regexVar.test(string) || string == "E" || string == "PI" || string == "-E" || string == "-PI");
    }
    
    this.isVariable = function(string){
        var regexVar = /^x\d+$/;
        return regexVar.test(string);
    }
}

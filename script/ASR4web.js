// JavaScript Document


//Definov�n� funkc�, na�ten� konfigurace, registrov�n� event�, zapo�et� p�ipojov�n�
    function WebASR(config_url, err){
        
           var ws;
           var sip_asr;
           var coolPhone;
           var rtcSession;
           //var on_grammarEvent; 
           
           var jsonError = function(){
               err.call(this, new Error("JSON not connected"));
           }

           //Funkce pro zapnut� rozpozn�v�n�
           this.recognize = function () {   
                if (ws != null) {
                    ws.send({type: "asr_recognize"});
                }
           }
           //Funkce pro vypnut� rozpozn�v�n�
           this.pause = function () {   
                if (ws != null) {
                    ws.send({type: "asr_pause"});
                }  
           }
           //Funkce pro zm�nu enginu
           this.switch_engine = function (model) {   
                if (ws != null) {
                    ws.send({type: "asr_switch_engine", engine: model});
                }  
           }   
           //Funkce pro zm�nu gramatiky
           this.set_grammar = function (grammar_type, grammar) {   
                ws.send({type: "asr_set_grammar", grammar_type: grammar_type, grammar: grammar});
            }

           //Definov�n� funkce p�i v�sledku
           this.on_result=function(func){
                on_resultEvent=func;
           }
           //Definov�n� funkce p�i nov� informaci o sign�lu
           this.on_signal=function(func){                      
                on_signalEvent=func;
           }
           //Definov�n� funkce p�i zm�n� stavu p�ipojen�
           this.on_state_change=function(func){                      
                on_state_changeEvent=func;
           }
           
           //Definov�n� funkce pro v�pis engin�
           this.on_get_list_engines=function(func){                      
                on_get_list_enginesEvent=func;
           }

           //Function for grammar callback func(ok, msg) (ok is true/false, msg is string)
           this.on_grammar=function(func){
                on_grammarEvent=func;
           }
           
           //Funkce pro form�tov�n� v�sledku
           this.escapeHTML = function (b) {
                var a = $("<div />");
                a.text(b);
                return a.html()
           }
           
           //Vytvo�en� SIP hovoru
           var make_call = function(){
              var eventHandlers = {
                'progress':   function(e){ /* Your code here */ },
                'failed':     function(e){ 
                    rtcSession = null;
                    if(on_state_changeEvent)on_state_changeEvent(e);
                },
                'started':    function(e){
                    rtcSession = e.sender;
                    if(on_state_changeEvent)on_state_changeEvent(e);
                },
                'ended':      function(e){ rtcSession = null;}
              };
  
              var options = {
                'eventHandlers': eventHandlers,
                'extraHeaders': [ 'X-Foo: foo', 'X-Bar: bar' ],
                'mediaConstraints': {'audio': true, 'video': false }
              };
  
              coolPhone.call(sip_asr, options);
            }
        
            //P�ipojen� p�es WebSocket
            function ws_connect(addr) {
                if (ws) {
                    ws.close();
                }
    
                ws = new WebSocket(addr);
    
                ws.send_str = ws.send;
                ws.send = function(data) {
                    s = JSON.stringify(data)
                    console.log("Sending " + s);
                    if(on_state_changeEvent)on_state_changeEvent("Sending " + s);
                    ws.send_str(s);
                }
    
                //Registrov�n� ud�lost�
                ws.onmessage = function(evt) {
                    var data = jQuery.parseJSON(evt.data);
    
                    if (data.type == 'asr_result') {
                        if(on_resultEvent)on_resultEvent(data);
                        console.log(JSON.stringify(data));
                    }
                    else if (data.type == 'asr_signal') {  
                        if(on_signalEvent)on_signalEvent(data);
                    } 
                    else if (data.type == 'asr_listing_engines') {  
                        console.log(data.engines);
                        if(on_get_list_enginesEvent)on_get_list_enginesEvent(data.engines);
                    } 
                    else if (data.type == 'asr_set_grammar_ok') {
                        console.log(data);
                        if (on_grammarEvent) on_grammarEvent(true, "");
                    }
                    else if (data.type == 'asr_set_grammar_error') {
                        console.log(data);
                        if (on_grammarEvent) on_grammarEvent(false, data.msg);
                    }
                };
    
                ws.onclose = function(evt) {
                    console.log("Connection not initialized!!!"); 
                    if(on_state_changeEvent)on_state_changeEvent("Connection not initialized!!!");
                };
    
                ws.onopen = function(evt) { 
                    console.log("Initialization OK");
                    if (ws != null){
                      ws.send({type: "asr_pause"});
                      ws.send({type: "asr_list_engines"});
                    } 
                    if(on_state_changeEvent)on_state_changeEvent("Initialization OK");
                    
                };
            }
           
            $.getJSON(config_url, function(data) {
                  //Na�ten� konfigurace
                  var configuration = {
                      'ws_servers': data.websocket_proxy_url,
                      'uri': 'sip:'+data.username+'@'+data.domain,
                      'password': data.password,
                      'stun_servers': data.stun_servers,
                  };
                  sip_asr = data.server;
                  console.log("loaded JSON, server " + data.domain + ", username " + data.username);
                  if(on_state_changeEvent)on_state_changeEvent("Loaded JSON, server " + data.domain + ", username " + data.username);
  
                  //Konfigurace a p�id�n� eventr� SIP telefonu
                  coolPhone = new JsSIP.UA(configuration);
  
                  coolPhone.on('disconnected', function(e){ 
                      if (rtcSession) {
                          rtcSession.terminate();
                      } 
                  });
                  
                  coolPhone.on('newMessage', function(e){
                      content = e.data.request.body;
                      if (content.substring(0, 2) == "ws") {
                          console.log("connecting WebSocket " + content);
                          if(on_state_changeEvent)on_state_changeEvent("Connecting WebSocket " + content);
                  		    ws_connect(content);
                      }
                  });
                  
                 // coolPhone.on('connected', function(e){ make_call() });
                  coolPhone.on('registered', function(e){ make_call() });
                  
                  coolPhone.start();
            }).fail(function() {
                  jsonError();
            });
    }

function setup_calcul(){
    config_url = 'http://asr4web.kky.zcu.cz:15051/config/asr4sip.json';
    $("#volume").hide();
    $("#recognize").hide();
    $("#pause").hide();
    
    //Konstruktor WebASR
    var asr = new WebASR(config_url, function(e) {
         $("#appStatus").removeClass("load");
         $("#appStatus").addClass("error");
         console.log(e);
    });
    
    var recognize_index = 1;
    
    var calcul = new calculBrain();
    
    //Po přijetí výsledku, vypiš výsledek v různých formátech
    asr.on_result(function(data){
        console.log(data.result);
        
        var text = data.text.clean_text.replace( /( |^)(f|ch|š|h)( |$)/g, '');
        $("#content").prepend("<div id='recognize"+recognize_index+"'><span class='rec'>"+text+"</span></div>");
        var this_index = recognize_index;
        recognize_index++;
        
        var tagsIn = [];
        data.tags.forEach(function(tag){
            allTags = tag.split("_");
            allTags.forEach(function(splitTag){
                tagsIn.push(splitTag);
            });
        });
        tagsIn.forEach(function(tag, index){
           if(tag == "null"){
               tagsIn.splice(index, 1);
           }
        });
        var responses = calcul.sendData(tagsIn);
        setTimeout(function(){writeResults(responses, this_index)}, 3000);
    });
    
    function writeResults(responses, index){
            var rec = $("#content #recognize"+index);
            rec.hide();
            var text = rec.find('.rec').html();
            responses.forEach(function(response){
                if(response.valid){
                    rec.before("<div><span class='success calcul' title='"+response.lisp+"' onClick='$(\"#content #recognize"+index+"\").toggle();'>$"+response.tex+"$</span></div>");
                }else{
                    console.log(response.tree);
                    rec.before("<div><span class='error calcul' title='"+response.error+"'>"+text+"</span></div>");
                }
            });
            jsMath.ConvertTeX();
            jsMath.ProcessBeforeShowing();
    }
    
    //Po stisku tlačítka ,,recognize" začni rozpoznávat
    $("#recognize").click(function(evt) { 
        asr.recognize();
        $("#volume").show();
        $(this).hide();
        $("#pause").show();
    });

    //Po stisku tlačítka ,,pause" přestaň rozpoznávat
    $("#pause").click(function(evt) {
        asr.pause();  
        $("#volume").hide();
        $(this).hide();
        $("#recognize").show();
    });

    //Po přijetí zprávy o změně stavu, vypiš zprávu
    asr.on_state_change(function(data){
        //__log("status",data);
    });
        
    //Po přijetí zprávy o signálu, změň ikonku
    asr.on_signal(function(data){
        if(data.speech && $("#volume").hasClass("low")){
            $("#volume").removeClass("low");
            $("#volume").addClass("ok");
        }else if(!data.speech && $("#volume").hasClass("ok")){
            $("#volume").removeClass("ok");
            $("#volume").addClass("low");
        }
        $("#volume").attr("title", data.level);
    }); 

    //Po přijetí zprávy o registraci gramatiky, vypiš zprávu
    asr.on_grammar(function(ok, msg){
        if(ok){
            $("#appStatus").removeClass("load");
            $("#appStatus").addClass("ok");
            document.title = 'Online kalkulačka';
            $("#recognize").show();
        }else{
            $("#appStatus").removeClass("load");
            $("#appStatus").addClass("error");
            document.title = 'Online kalkulačka - chyba';
        }
        console.log("set_grammar "+ok);
    }); 
    
    //Po přijetí listu režimů, prepni na spravny
    asr.on_get_list_engines(function(){
        $.ajax({
            url: "grammar.esgf",
            success: function(grammar){
                asr.switch_engine("GRM_ML");
                asr.set_grammar('esgf', grammar);  
            },
            error: function() {
                $("#appStatus").removeClass("load");
                $("#appStatus").addClass("error");
            },
            cache: false
        });
    });
}